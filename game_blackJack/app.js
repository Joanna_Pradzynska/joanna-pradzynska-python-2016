$(document).ready(function() {
	function makeDeck() {
		var colors = ['♣', '♠', '♥', '♦']; 
		var	figures = ['A', 'K', 'Q', 'J'];
		var	deck = [];
			
		for (var i = 2; i <= 10; i++) {
			figures.push(i.toString());
		}

		for (var i in figures) {
			for (var j in colors) {
				deck.push(figures[i] + " " + colors[j]);
				// console.log(figures[i], colors[j]);
			}
		}
		return deck;
	}
	
	function shuffleArray(arr) {
		var rand;
		var temp;

		for (var i=arr.length; i>0; i--) {
			rand = (Math.random() * i) | 0;
			temp = arr[i - 1];
			arr[i - 1] = arr[rand];
			arr[rand] = temp;
		}
		return arr;
	}

	var deck = makeDeck();
	var computerCard;
	var playerCard;
	

	startNewGame();

	function startNewGame() {
		$('.card').removeClass('winner');
		shuffleArray(deck);
		playerCard = deck.pop();
		computerCard = deck.pop();	
		$('.card').html('').addClass('cardBack');
		$('.button').html('<button id="start">Odkryj karty</button>');	
	}

	$('.container').on('click', '#start', function() {

		$('.card').removeClass('cardBack').addClass('cardFront');
		$('.card:nth-child(1)').html('<div class="top-left">' + playerCard + '</div><div class="bottom-right">' + playerCard + '</div>');
		$('.card:nth-child(2)').html('<div class="top-left">' + computerCard + '</div><div class="bottom-right">' + computerCard + '</div>');
		// console.log(playerCard, computerCard);	

		checkWinner();	

		$('.button').html('<button id="reStart">Zagraj ponownie</button>');

	});

	$('.container').on('click', '#reStart', function() {
		startNewGame();		
	});

	function checkWinner() {

		var playerNumb = getCardValue(playerCard);
		var computerNumb = getCardValue(computerCard);
		// console.log(playerNumb + " | " + computerNumb);

		if (playerNumb > computerNumb) {
			$('.card:nth-child(1)').addClass('winner');
			$('.card:nth-child(1) > .top-left').after('<div class="message">ZWYCIĘZCA!</div>');

		}
		else if (computerNumb > playerNumb) {
			$('.card:nth-child(2)').addClass('winner');
			$('.card:nth-child(2) > .top-left').after('<div class="message">ZWYCIĘZCA!</div>');
		}
		else {
			$('.card').addClass('winner');
			$('.top-left').after('<div class="message">REMIS!</div>');
		}
	}

	function getCardValue(card) {

		var cardValue = card.substring(0, card.length-1).trim();

		switch(cardValue) {
		    case 'A':
		    case 'K':
		    case 'Q':
		    case 'J':
		        return 11;
		    default:
		        return parseInt(card);
		}
	}

});