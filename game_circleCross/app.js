$(document).ready(function() {
    
    var currentSymbol = 'O';
    var usedIndexes = [];
    
    $('.field').click(function() {
        
//        console.log("Kliknięto w " + $(this).data('pos')); 
        
        var id = parseInt($(this).data('pos'));
        
        if (usedIndexes.indexOf(id) == -1) {
            
            $(this).html(currentSymbol);
            usedIndexes.push(id);
        }

        checkGameStatus();  

        currentSymbol = (currentSymbol == 'O') ? 'X' : 'O';

//        if (currentSymbol == "O") {
//            currentSymbol = "X";
//        }
//        else {
//            currentSymbol = "O";
//        }
    });

    function checkGameStatus() {

        var isWinner = checkForWin();

        if (isWinner != false) {       
            for (var i=0; i< isWinner.length; i++ ) {
                $('[data-pos=' + isWinner[i] + ']').css('background-color', "lightgreen");
            }
            winner();
        }
        else if (usedIndexes.length == 9) {
            tie();
        }  
    }

    function winner() {
        $('.gameResult').html('BRAWO! Wygrał gracz: ' + currentSymbol + '<br /><button id="newGame">Zagraj ponownie</button>');
        
    }

    function tie() {
        $('.gameResult').html('REMIS!<br /><button id="newGame">Zagraj ponownie</button>');
    }

    $('.gameResult').on('click', '#newGame', function() {
        resetVars();
        $('.gameResult').html("");
    });

    function resetVars() {
        usedIndexes = [];
        $('.field').html("");    
        for (var i=1; i < 10; i++ ) {
            $('[data-pos=' + i + ']').css('background-color', "#ffffe6");
        }
        // $('.field').css('background-color', "#ddccff");
    }
    
	function checkForWin() {
		var winPosibilities = [
			[1, 2, 3],
			[4, 5, 6],
			[7, 8, 9],
			[1, 4, 7],
			[2, 5, 8],
			[3, 6, 9],
			[1, 5, 9],
			[3, 5, 7]
		];
        for(var i = 0; i < winPosibilities.length; i++) {
			var currSearch = winPosibilities[i];
			var elem = [];
			var cs = currentSymbol;

//        console.log(currSearch[0], currSearch[1], currSearch[2]);
			elem.push(  $('[data-pos=' + currSearch[0] + ']').html()  );
			elem.push(  $('[data-pos=' + currSearch[1] + ']').html()  );
			elem.push(  $('[data-pos=' + currSearch[2] + ']').html()  );
			//console.log(elem);
			if(elem[0] == cs && elem[1] == cs && elem[2] == cs) {
				return currSearch;
			}
		}
		return false;
	}
 
});