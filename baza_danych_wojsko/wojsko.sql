-- Zaprojektuj bazę danych dla wojska. Baza danych powinna zawierać spis pracowników, ewidencje sprzętu, rozpisane misje, podział żołnierzy oraz relacje pomiędzy nimi (tj. np. tabela odpowiadająca za drużyny powinna przetrzymywać tylko id żołnierzy, a nie ich wszystkie dane (imię / nazwisko).


-- BAZA DANYCH DLA WOJSKA


-- TABELA RODZAJE_SAL

CREATE TABLE rodzaje_sal (
  id               INT         NOT NULL,
  rodzaj           VARCHAR(30) NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO rodzaje_sal
VALUES (1, 'DYZURKA'),
       (2, 'POMIESZCZENIE TECHNICZNE'),
       (3, 'SEKRETARIAT'),
       (4, 'ADMINISTRACJA'),
       (5, 'GABINET LEKARSKI');
       

-- TABELA SALE

CREATE TABLE sale (
  id               INT        NOT NULL,
  numer            VARCHAR(6) NOT NULL,
  id_rodzaj        INT        NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY(id_rodzaj) REFERENCES rodzaje_sal(id)
);

INSERT INTO sale
VALUES (1, '1', 1), 
       (2, '10', 2),
       (3, '111A', 3),
       (4, '111B', 4),
       (5, '120', 5);


-- TABELA STANOWISKA

CREATE TABLE stanowiska (
  id               INT         NOT NULL,
  stanowisko       VARCHAR(25) NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO stanowiska
VALUES (1, 'SEKRETARIAT'), 
       (2, 'ADMINISTRACJA'),
       (3, 'OCHRONIARZ'), 
       (4, 'SPRZATACZKA'),
       (5, 'LEKARZ');
       

-- TABELA PRACOWNICY

CREATE TABLE pracownicy (
  id               INT           NOT NULL,
  imie             VARCHAR(15)   NOT NULL,
  nazwisko         VARCHAR(20)   NOT NULL,
  id_stanowisko    INT           NOT NULL,
  id_sala          INT           NOT NULL,
  data_zatr        DATE          NOT NULL,    
  pensja           DECIMAL(8,2),
  dodatek          DECIMAL(8,2),
  telefon          VARCHAR(16),
  PRIMARY KEY(id),
  FOREIGN KEY(id_stanowisko) REFERENCES stanowiska(id),
  FOREIGN KEY(id_sala) REFERENCES sale(id)
);

INSERT INTO pracownicy
VALUES (1, 'JAN', 'KOWALSKI', 3, 1, '2000-05-10', 2000, 10, '123456789'),
       (2, 'ZYGMYNT', 'LUTY', 3, 1, '2001-10-01', 2000, 50, '541786369'),
       
       (3, 'MARIA', 'KOWALSKA', 1, 3, '1995-01-01', 3900, 100, '444888556'),
       (4, 'KAROLINA', 'CZAJ', 1, 3, '2002-02-15', 3500, 80, '478874478'),
       (5, 'IWONA', 'MAJ', 2, 4, '2003-11-14', 3800, 140, '111222333'),
       (6, 'MONIKA', 'LIPA', 2, 4, '2000-09-02', 3400, 120, '444555666'),
       
       (7, 'ANNA', 'NOWAK', 4, 2, '1989-01-01', 2000, 0, '753951258'),
       (8, 'WANDA', 'DROGA', 4, 2, '2006-03-22', 2500, 50, '456258741'),
       
       (9, 'MAGDALENA', 'NOWACKA', 5, 5, '2001-02-11', 4500, 50, '124789909');


-- TABELA RANGI

CREATE TABLE rangi (
  id               INT         NOT NULL,
  stanowisko       VARCHAR(30) NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO rangi
VALUES (1, 'SZEREGOWY'),
       (2, 'STARSZY SZEREGOWY'),
       (3, 'KAPRAL'),
       (4, 'STARSZY KAPRAL'),
       (5, 'PLUTONOWY'),
       (6, 'SIERZANT'),
       (7, 'STARSZY SIERZANT'),
       (8, 'MLODSZY CHORAZY'),
       (9, 'CHORAZY'),
       (10, 'STARSZY CHORAZY'),
       (11, 'STARSZY CHORAZY SZTABOWY'),
       (12, 'PODPORUCZNIK'),
       (13, 'PORUCZNIK'),
       (14, 'KAPITAN'),
       (15, 'MAJOR'),
       (16, 'PODPULKOWNIK'),
       (17, 'PULKOWNIK'),
       (18, 'GENERAL BRYGAYDY'),
       (19, 'GENERAL DYWIZJI'),
       (20, 'GENERAL BRONI'),
       (21, 'GENERAL');


-- TABELA MIEJSCA

CREATE TABLE miejsca (
  id               INT         NOT NULL,
  miejscowosc      VARCHAR(30) NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO miejsca
VALUES (1, 'WISLA'),
       (2, 'BOZEPOLE MALE'),
       (3, 'POZNAN'),
       (4, 'GDANSK'),
       (5, 'KRAKOW'),
       (6, 'KIELCE');


-- TABELA SPIS_ZOLNIERZY

CREATE TABLE spis_zolnierzy (
  id                INT           NOT NULL,
  imie              VARCHAR(15)   NOT NULL,
  nazwisko          VARCHAR(20)   NOT NULL,
  data_zatr         DATE          NOT NULL,    
  pensja            DECIMAL(8,2),
  dodatek           DECIMAL(8,2),
  telefon           VARCHAR(16),
  PRIMARY KEY(id)
);

INSERT INTO spis_zolnierzy
VALUES (1, 'jakub', 'WROTKA', '2014-03-01', 7950, 150, '457845222'),
       (2, 'RYSZARD', 'KOWALSKI', '2014-06-01', 8000, 200, '456985365'),
       (3, 'WOJCIECH', 'CIABATA', '2014-03-01', 7500, 120, '478598777'),
       (4, 'BARTLOMIEJ', 'OCZKO', '2014-03-01', 10000, 90, '445588996'),
       (5, 'DANIEL', 'USZATY', '2014-06-01', 11000, 220, '745896325'),
       (6, 'ARTUR', 'WIELKI', '2014-03-01', 12000, 200, '125698756'),
       (7, 'PIOTR', 'SKORA', '2014-06-01', 17000, 150, '145875632'),
       (8, 'ZBIGNIEW', 'GAZDZINA', '2014-03-01', 20000, 110, '654875654');


-- TABELA PRZYNALEZNOSCI_DO_RANGI

CREATE TABLE przynaleznosc_do_rangi (
  id               INT NOT NULL,
  id_zolnierz      INT NOT NULL,
  id_ranga         INT NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY(id_zolnierz) REFERENCES spis_zolnierzy(id)
);

INSERT INTO przynaleznosc_do_rangi
VALUES (1, 1, 1),
       (2, 2, 1),
       (3, 3, 1),
       (4, 4, 3),
       (5, 5, 8),
       (6, 6, 14),
       (7, 7, 15),
       (8, 8, 20);


-- TABELA ODDZIALY

CREATE TABLE oddzialy (
  id               INT NOT NULL,
  id_zolnierz      INT NOT NULL,
  id_miejsce       INT NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY(id_zolnierz) REFERENCES spis_zolnierzy(id),
  FOREIGN KEY(id_miejsce) REFERENCES miejsca(id)
);

INSERT INTO oddzialy
VALUES (1, 1, 1),
       (2, 2, 1),
       (3, 3, 2),
       (4, 4, 3),
       (5, 5, 4),
       (6, 6, 5),
       (7, 7, 6),
       (8, 8, 5);
       
       
-- TABELA TYP_SPRZETU

CREATE TABLE typ_sprzetu (
  id          INT         NOT NULL,
  opis        VARCHAR(25) NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO typ_sprzetu
VALUES (1, 'CZOLG'),
       (2, 'KARABIN'),
       (3, 'GRANAT'),
       (4, 'MINA'),
       (5, 'KOMPUTER'),
       (6, 'ARMATA'),
       (7, 'HELIKOPTER'),
       (8, 'SAMOCHOD'),
       (9, 'LODZ PODWODNA'),
       (10, 'NOZ');
       

-- TABELA EWIDENCJA_SPRZETU

CREATE TABLE ewidencja_sprzetu (
  id               INT NOT NULL,
  id_typ_sprzetu   INT NOT NULL,
  id_oddzial       INT NOT NULL,
  id_zolnierz      INT NOT NULL,
  data_przekazania DATE NOT NULL,
  data_zwrotu      DATE,
  PRIMARY KEY(id),
  FOREIGN KEY(id_zolnierz) REFERENCES spis_zolnierzy(id),
  FOREIGN KEY(id_oddzial) REFERENCES oddzialy(id)
);

INSERT INTO ewidencja_sprzetu
VALUES (1,1,1,1, '2014-04-01', NULL),
       (2,10,1,2, '2014-07-01', NULL),
       (3,2,2,3, '2014-04-11', NULL),
       (4,3,3,4, '2014-04-23', NULL),
       (5,3,4,5, '2014-07-01', NULL),
       (6,4,5,6, '2014-04-24', NULL),
       (7,5,6,7, '2014-07-01', NULL),
       (8,6,5,8, '2014-05-11', NULL),
       (9,7,1,1, '2014-04-01', NULL),
       (10,7,1,1, '2014-04-01', NULL),
       (11,8,1,2, '2014-07-01', NULL),
       (12,9,4,3, '2014-04-12', NULL),
       (13,4,5,4, '2014-05-01', NULL);