$(document).ready(function() {
   
    var wordsDictionary = ['oko', 'konik', 'hangar', 'owca', 'skuter', 'kwiaty', 'balon', 'muzyka'];
    var currentWord = wordsDictionary[(Math.random() * wordsDictionary.length) | 0].toLowerCase();
    var currentUserWord = shadowWord(currentWord);
    var usedLetters = [];
    var totalTries = 0;
    var triesLimit = uniqueLetters(currentWord) + 1;

    countLives(triesLimit, totalTries);
    
    console.log(currentWord);

    $('.wordCount').html(currentUserWord);
       
    $('.letter').click(function() {
        
        var currentLetter = $(this).val().toLowerCase();
        var currentLetterIndexes = [];

        if (usedLetters.indexOf(currentLetter) == -1) {
            usedLetters.push(currentLetter);
            totalTries ++;
            countLives(triesLimit, totalTries);
        }
        
        $('.usedLetters').html("&nbsp;&nbsp;" + usedLetters.join(", "));

        for (var i = 0; i < currentWord.length; i++) {
            if (currentWord[i] == currentLetter) {
                console.log('Zgadnięto literę ' + currentLetter + ' pod indeksem ' + i);
                currentLetterIndexes.push(i);
            }
        }
        console.log(currentLetterIndexes);
        if (currentLetterIndexes.length == 0) {
            $(this).attr('disabled', 'disabled').css({'background-color' : '#f5f5f0', 'border' : '0'}).val("");
        }
        currentUserWord = unhideLetters(currentUserWord, currentLetter, currentLetterIndexes);

        if (currentUserWord == currentWord) {
            $('.wordCount').html(currentUserWord);
            winner();
        }
        else if (totalTries == triesLimit) {
            looser(currentWord);
        }
        else {
            $('.wordCount').html(currentUserWord);
        }
    });  

    $('#guess').click(function() {

        totalTries ++;
        countLives(triesLimit, totalTries);

        var writtenWord = $('#guessedWord').val();

        if (writtenWord == currentWord) {
            $('.wordCount').html(writtenWord);
            winner();
        }
        else if (totalTries == triesLimit) {
            looser(currentWord);
        }
    });

});

function shadowWord(str) {
    return "_".repeat(str.length);
}

function unhideLetters(sentence, letter, indexes) {
    var tempArr = sentence.split("");
    if(indexes.length > 0) {
        for(var i in indexes) {
            tempArr[indexes[i]] = letter;
        }
    }
    return tempArr.join("");
}

function winner() {
    $('.alphabet').css({'display' : 'none'});
    $('.guessBox').html('BRAWO!<br />Wygrana :)<br /><img src="./winner.gif" style="width:60%;" />').css({'display' : 'none'}).addClass('winner text-center').fadeIn(2000);
}
function countLives(limit, used) {
    var leftLives = limit - used;
    $('.choicesLeft').html(leftLives);
}
function looser(word) {
    $('.alphabet').css({'display' : 'none'});
    $('.guessBox').html('PRZEGRAŁEŚ!<br /><img src="./looser.gif" />').css({'display' : 'none'}).addClass('looser text-center').fadeIn(2000);
    $('.wordCount').html(word).css({'color' : 'red'});
}
function uniqueLetters(str) {
    var tempTab = str.split("");
    var unique = [];
    for (var i in str) {
        if (unique.indexOf(tempTab[i]) == -1) {
            unique.push(tempTab[i]);
        }
    }
    return unique.length;
}