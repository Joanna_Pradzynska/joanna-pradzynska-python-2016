//  metoda iteracyjna!
function wyliczSilnie(silnia) {
  var wynik=1;

  for (var i=1; i<=silnia; i++) {
    wynik *= i;
    document.write("i = " + i + " -> " + wynik + "<br />");
  }
  return wynik;
}
var numb = 5;
document.write(numb + "! = " + wyliczSilnie(numb));


/*  rekursja!

function silniaRek(silnia) {
  if (silnia<2) {
    return 1;
  }  
  return silnia*silniaRek(silnia-1);
}

document.write(silniaRek(0));

*/


/*
var wynik = 1;
var silnia = 5;

for (var i = 1; i <= silnia; i++) {
  wynik *= i;
  document.write("i = " + i + ", wynik = " + wynik + "<br />");
}
document.write(silnia + "! = " + wynik);
*/

/*
var num=5;
var silnia=1;

if (num == 0) {
  document.write(silnia);
}
else {
  for (var i=0; i<num; i++) {
    silnia *=(num-i);
  }
  document.write(silnia);
}
*/