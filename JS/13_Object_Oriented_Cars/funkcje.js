// OBIEKTOWOŚĆ!

function Samochod(mark, color, year) {
    this.marka = mark;
    this.kolor = color;
    this.rocznik = year;

    this.desc = function () {
        var desc = 'Samochód marki: ' + this.marka + '<br/>';
        desc += 'w kolorze: ' + this.kolor + '<br/>';
        desc += 'wyprodukowany w roku: ' + this.rocznik + '<br/>';
        desc += '<br/>';
        document.write(desc);
    };

    this.zmienKolor = function (color) {      // zmiana koloru auta
        alert('Zmieniono kolor auta marki: ' + this.marka + ' z ' + this.kolor + ' na ' + color);
        this.kolor = color;
        document.write('Nowy kolor auta marki: ' + this.marka + ' to kolor: ' + color + '<br/>');
    };

    this.zmienRokProdukcji = function (newYear) {      // zmiana rocznika
        alert('Zmieniono rok produkcji auta marki: ' + this.marka + ' z ' + this.rocznik + ' na ' + newYear);
        this.rocznik = newYear;
        document.write('Poprawny rok produkcji auta marki: ' + this.marka + ' to rok: ' + newYear + '<br/>');
    };
}

var sam1 = new Samochod('Mercedes', 'Srebrny', 2000);
var sam2 = new Samochod('Toyota', 'Biały', 2015);
var sam3 = new Samochod('Peugeot', 'Czarny', 2010);
var sam4 = new Samochod('Hyundai', 'Szary', 2013);

sam1.desc();
sam2.desc();
sam3.desc();
sam4.desc();

sam2.zmienKolor('Zielony');
console.log(sam4.rocznik);
sam4.zmienRokProdukcji(2005);
console.log(sam4.rocznik);