for (var rowN=0; rowN<5; rowN++) {
   for (var colN=0; colN<10; colN++) {
  	   document.write('*');
   }
   document.write("<br />");
}

document.write("<br /><br />");
// alternatywne rozwiązanie
var i=0;
while (i<5) {
	var j=0;
	while (j<10) {
		document.write('*');
  		j++;
	}
  document.write("<br />");
  i++;
}

document.write("<br /><br />");
// alternatywne rozwiązanie
var i=0;
var j=0;
while (i<5) {
	while (j<10) {
		document.write('*');
  		j++;
	}
    j=0;
  document.write("<br />");
  i++;
}
