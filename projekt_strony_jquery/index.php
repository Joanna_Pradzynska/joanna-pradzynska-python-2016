<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>jQuery</title>
      
        <link rel="stylesheet" href="css/style.css" type="text/css" />
        <link rel="stylesheet" href="js/owl/owl.carousel.css" type="text/css" />
        <link rel="stylesheet" href="js/owl/owl.theme.css" type="text/css" />
        <link rel="stylesheet" href="js/owl/owl.transitions.css" type="text/css" />
        <script type="text/javascript" src="js/jquery-3.1.0.min.js"></script>
        <script type="text/javascript" src="js/owl/owl.carousel.js"></script>
        <script type="text/javascript" src="js/jquery.validate.js"></script>
        <script type="text/javascript" src="js/functions.js"></script>
    </head>
    
    <body>
        
        <div id="wrapper">
             <div id="header" class="container">
                <div id="menu">
                    <ul>
                        <li>
                            <a href="http://google.pl">Element 1</a>
                            <ul>
                                <li><a href="#">PodElement 1</a></li>
                                <li><a href="#">PodElement 2</a></li>
                                <li><a href="#">PodElement 3</a></li>
                                <li><a href="#">PodElement 4</a></li>
                                <li><a href="#">PodElement 5</a></li>
                            </ul>
                        </li>
                        <li><a href="http://google.pl">Element 2</a></li>
                        <li>
                            <a href="http://google.pl">Element 3</a>
                            <ul>
                                <li><a href="#">PodElement 1</a></li>
                                <li><a href="#">PodElement 2</a></li>
                                <li><a href="#">PodElement 3</a></li>
                                <li><a href="#">PodElement 4</a></li>
                                <li><a href="#">PodElement 5</a></li>
                            </ul>
                        </li> 
                        <li><a href="http://google.pl">Element 4</a></li>
                        <li><a href="http://google.pl">Element 5</a></li>
                    </ul>
                </div>
            </div>
            
            
            <div id="content" class="container">
                <h1 class="title">Tytuł Strony</h1>
                <div id="leftColumn" class="column">
                    <ul class="tabs">
                        <li class="active"><a href="#" data-rel="tab1">Tab 1</a></li>
                        <li><a href="#" data-rel="tab2">Tab 2</a></li>
                        <li><a href="#" data-rel="tab3">Tab 3</a></li>
                        <li><a href="#" data-rel="tab4">Tab 4</a></li>
                        <li><a href="#" data-rel="tab5">Tab 5</a></li>
                    </ul>
                    <div id="tab1" class="tab active">Treść 1</div>
                    <div id="tab2" class="tab">Treść 2</div>
                    <div id="tab3" class="tab">Treść 3</div>
                    <div id="tab4" class="tab">Treść 4</div>
                    <div id="tab5" class="tab">Treść 5</div>
                </div>
                
                <div id="rightColumn" class="column">
                    <div id="owl-demo" class="owl-carousel owl-theme">
                        <div class="item"><img src="img/slides/photo1 680_460.jpg" alt=""></div>
                        <div class="item"><img src="img/slides/photo2 680_460.jpg" alt=""></div>
                        <div class="item"><img src="img/slides/photo3 680_460.jpg" alt=""></div>
                        <div class="item"><img src="img/slides/photo4 680_460.jpg" alt=""></div>

                    </div>
                </div>
            </div>
            
            
            <div id="footer" class="container">
                <div id="owl-example" class="owl-carousel">
                    <div>Content 1</div>
                    <div>Content 2</div>
                    <div>Content 3</div>
                    <div>Content 4</div>
                    <div>Content 5</div>
                    <div>Content 6</div>
                    <div>Content 7</div>
                    <div>Content 8</div>
                    <div>Content 9</div>
                    <div>Content 10</div>
                </div>
                
                
                <div class="contactForm">
                    <form method="POST">
                    
                        <div class="row">
                            <label for="subject">Temat:</label>
                            <input placeholder="Temat" id="subject" type="text" name="contact[subject]" class="text" value="" />
                        </div>

                        <div class="row">
                            <label for="name">Imię i Nazwisko:</label>
                            <input placeholder="Imię i Nazwisko" id="name" type="text" name="contact[name]" class="text" value="" />
                        </div>

                        <div class="row">
                            <label for="email">Email:</label>
                            <input placeholder="Email" id="email" type="text" name="contact[email]" class="text" value="" />
                        </div>

                        <div class="row">
                            <label for="contactContent">Treść:</label>
                            <textarea id="contactContent" name="contact[content]" class="text" >Treść</textarea>
                        </div>

                        <div class="row submit">
                            <input id type="submit" class="button" value="Wyślij" />
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
        
        
        <div id="slidingBox">
            <div class="label">
                E
            </div>
            <div class="content">
                <a href="http://google.pl" target="_blank">Link do google.com</a>
            </div>
        </div>
        
    </body>
</html>
