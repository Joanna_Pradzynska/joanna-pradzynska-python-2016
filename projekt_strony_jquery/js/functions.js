$(document).ready(function(){  

    $('#slidingBox a').click(function(e) {

        if ($(this).hasClass('clicked')) {
            $(this).text('Zapytaj jeszcze raz').removeClass('clicked');
        }
        else {
            $(this).text('Na pewno?').addClass('clicked');
            e.preventDefault(); 
        }
    });


    $('#slidingBox .label').click(function(){

        if ($(this).hasClass('open')) {
            $(this).removeClass('open');
            $(this).parent().animate({'right': -248}, 1000);
        }
        else { 
            $(this).addClass('open');
            $(this).parent().animate({'right': 0}, 1000);
        }
        
    });


    $('#menu > ul > li').on('click', 'a', function(e) {

        if ($(this).siblings().length > 0) {
            e.preventDefault();
            
            if ($(this).hasClass('showList')) {
                $(this).removeClass('showList');
                $(this).siblings('ul').slideUp("slow");
            }
            else {
                $(this).addClass('showList');
                $(this).siblings('ul').slideDown("slow");
            }

        }

    });    
    
    $('ul.tabs li a').click(function(){

        $(this).parents('ul').children().removeClass('active');
        $(this).parent().addClass('active');
        
        var rel = $(this).attr('data-rel');
        
        $('div.tab').each(function(){
            if ($(this).attr('id') == rel) {
                $(this).show();
            }
            else {
                $(this).hide();
            }
        });
        
    });

/////////////////////////////////////////////////////////////////////////////////////    

$("#owl-demo").owlCarousel({

    navigation : true,
    slideSpeed : 300,
    paginationSpeed : 400,
    singleItem:true,
    responsive : true,
    transitionStyle : "fade",


        // "singleItem:true" is a shortcut for:
        // items : 1, 
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

    });

$("#owl-example").owlCarousel();     

/////////////////////////////////////////////////////////////////////////////////////

    // $('.contactForm form').submit(function(e) {
    //     e.preventDefault();

    //     $(this).children().find('textarea, input').each(function() { 

    //       if ($(this).attr('type') != 'submit') {

    //         if ($(this).val() == '') {

    //             alert('Nie wypełniłeś pola ' + $(this).attr('placeholder'));  
    //             $(this).addClass('error');      
    //         }
    //         else {
    //             $(this).addClass('valid');
    //         }
    //       }

    //     });
    // });


    $('.contactForm form').validate({

        rules : {
            "contact[subject]" : {required : true, minlength: 3 },
            "contact[name]" : {required : true, minlength: 3},
            "contact[email]" : {required : true, email : true},
            "contact[content]" : { required : true, minlength: 20}
        },
        
        messages : {
            "contact[subject]" : "Wypełnij to pole",
            "contact[name]" : "Wypełnij to pole",
            "contact[email]" : "Wypełnij to pole",
            "contact[content]" : "Wypełnij to pole"
        }
        
    });
});