$(document).ready(function() {
   
    var questions = [
        
        {
          "question" : "Ile czasu potrzeba, aby przejść z punktu A do punktu B?",
          "answers" : ["5 godzin", "2 lata", "3 minuty", "Daj mi spokój, Hubert!"],
          "correctAnswer" : "2 lata",
          "tip" : "Więcej niż 3 minuty."
        },
        { 
          "question" : "Ile nóg ma kot Filemon?",
          "answers" : ["5", "2", "3", "4"],
          "correctAnswer" : "4",
          "tip" : "Parzysta liczba."
        },    
        { 
          "question" : "Ile wynosi 2 do potęgi 2?",
          "answers" : ["5", "około 4", "-1", "4"],
          "correctAnswer" : "4",
          "tip" : "Mniej niż 8."
        }        
    ];
    
    var usedIndexes = [];
    var currentQuestion = getRandomQuestion(questions, usedIndexes);
    fillData(currentQuestion);
    
	$('.friendHelp').click(function() {
		getHelpFromFriend(currentQuestion);
	});
	
	$('.fiftyFiftyHelp').click(function() {
		getHelpFromComputer(currentQuestion);
	});

	$('.audienceHelp').click(function() {
		getHelpFromAudience(currentQuestion);
	});


	$('.answer').click(function(){
		
		if (($(this).html()).substring(3) == currentQuestion["correctAnswer"]) {
		    console.log("BRAWO!");
			currentQuestion = getRandomQuestion(questions, usedIndexes);
			fillData(currentQuestion);
       } 
       else {
           console.log("ŹLE!");
           endGame();
       }
    });
    
});

// USED METHODS:

function fillData(obj) {

	getInputsUndisabled();
    
	var shuffledArr = getShuffledArray(obj["answers"]);

	$('.question').html(obj["question"]);   
	$('.answer').each(function(i, e) {
		var questLetter = $(this).data('letter').toUpperCase();
		e.innerHTML = questLetter + ") " + shuffledArr[i];
	});
}

function getRandomQuestion(questions, usedIndexes) {
	var index = getRandomIndex(questions, usedIndexes);
	if (index == undefined) {
		winGame();
	}
	return questions[index];
}

function getRandomIndex(questions, usedIndexes) {
	var elem = questions.length;
    var tempArr = [];
    var random;
    
	for(var i = 0; i<elem; i++) {
		if (usedIndexes.indexOf(i) == -1) {
            tempArr.push(i);
        }
    }
	random = (Math.random() * tempArr.length) | 0;
	usedIndexes.push(tempArr[random]);
    // console.log(usedIndexes);
	console.log("indeks: " + tempArr[random]);
	return tempArr[random];
}

function getInputsUndisabled() {
	$('.answer').removeAttr('disabled');
}
// function getInputsUndisabled() {
// 	$('.answer').each(function(i, e) {
// 		$(this).removeAttr('disabled');
// 	});
// }

function endGame() {
	$('.question, .answer').hide();
    alert("PRZEGRANA!");
}

function winGame() {
	$('.question, .answer').hide();
    alert("WYGRANA!");
}

function getShuffledArray(arr) {
	var arrLen = arr.length;
	var tempArr = [];

	while (arrLen > tempArr.length) {

		var randomIndex = (Math.random() * arr.length) | 0;

		if (tempArr.indexOf(arr[randomIndex]) == -1) {
			tempArr.push(arr[randomIndex]);
		}
	}
	return tempArr;
}

// GET HELP

function getHelpFromFriend(currentQuestion) {
	alert(currentQuestion["tip"]);
	$('.friendHelp').attr('disabled', 'disabled');
	return currentQuestion["tip"];
}

function getHelpFromComputer(currentQuestion) {
	var tempArr = [];
	// console.log(currentQuestion["correctAnswer"]);
	$('.answer').each(function(i, e){
		if (e.innerHTML != currentQuestion["correctAnswer"]) {
			tempArr.push(e);
		}
	});

	tempArr.pop().disabled = 'disabled';    // zabiera ostatni element z tablicy (pop) i go blokuje (disabled)
	tempArr.pop().disabled = 'disabled';
	$('.fiftyFiftyHelp').attr('disabled', 'disabled');
}

function getHelpFromAudience(currentQuestion) {
console.log(currentQuestion);
	var tempArr = [];
	var random;
	var sum = 0;

	var answers = $('.answer[disabled!="disabled"]');

	for (var i=0; i<answers.length; i++) {
		random = (Math.random() * 100) | 0;
		tempArr.push(random);
		sum += random;
	}

	tempArr = tempArr.map(function(num) {
		return (num*100/sum) | 0;
	});

	alert("Audience help: \nA) " + tempArr[0] + "%\nB) " + tempArr[1] + "%\nC) " + tempArr[2] + "%\nD) " + tempArr[3] + "%");
	$('.audienceHelp').attr('disabled', 'disabled');
}