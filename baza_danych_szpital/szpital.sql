-- Zaprojektuj bazę danych dla szpitala. Baza powinna uwzględniać m. in. pracowników, pacjentów, lekarzy, leki, sale, operacje oraz posiadać relację między nimi. Dodaj do bazy danych po 3-8 przykładowych rekordów.

-- BAZA DANYCH DLA SZPITALA


-- TABELA RODZAJE_SAL

CREATE TABLE rodzaje_sal (
  id               INT         NOT NULL,
  rodzaj           VARCHAR(30) NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO rodzaje_sal
VALUES (1, 'DYZURKA'),
       (2, 'GABINET LEKARSKI'),
       (3, 'GABINET ZABIEGOWY'),
       (4, 'SALA OPERACYJNA'),
       (5, 'SEKRETARIAT'),
       (6, 'POMIESZCZENIE TECHNICZNE');
       

-- TABELA SALE

CREATE TABLE sale (
  id               INT        NOT NULL,
  numer            VARCHAR(6) NOT NULL,
  id_rodzaj        INT        NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY(id_rodzaj) REFERENCES rodzaje_sal(id)
);

INSERT INTO sale
VALUES (1, '1A', 1), 
       (2, '112', 2),
       (3, '10', 4),
       (4, '113', 2),
       (5, '15', 3),
       (6, '2D', 6), 
       (7, '114', 2),
       (8, '115', 2),
       (9, '116', 2),
       (10, '117', 2),
       (11, '118', 2),
       (12, '119', 2);


-- TABELA STANOWISKA

CREATE TABLE stanowiska (
  id               INT         NOT NULL,
  stanowisko       VARCHAR(25) NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO stanowiska
VALUES (1, 'PIELEGNIARKA'), 
       (2, 'OCHRONIARZ'), 
       (3, 'SPRZATACZKA');


-- TABELA POZOSTALI_PRACOWNICY

CREATE TABLE pozostali_pracownicy (
  id               INT           NOT NULL,
  imie             VARCHAR(15)   NOT NULL,
  nazwisko         VARCHAR(20)   NOT NULL,
  id_stanowisko    INT           NOT NULL,
  id_sala          INT           NOT NULL,
  data_zatr        DATE          NOT NULL,    
  pensja           DECIMAL(8,2),
  dodatek          DECIMAL(8,2),
  telefon          VARCHAR(16),
  PRIMARY KEY(id),
  FOREIGN KEY(id_stanowisko) REFERENCES stanowiska(id),
  FOREIGN KEY(id_sala) REFERENCES sale(id)
);

INSERT INTO pozostali_pracownicy
VALUES (1, 'JAN', 'KOWALSKI', 2, 1, '2000-05-10', 2000, 10, '123456789'),
       (2, 'ZYGMYNT', 'LUTY', 2, 1, '2001-10-01', 2000, 50, '541786369'),
       (3, 'MARIA', 'ZASTRZYK', 1, 5, '1995-01-01', 3900, 100, '444888556'),
       (4, 'KAROLINA', 'ZMIANA', 1, 5, '2002-02-15', 3500, 80, '478874478'),
       (5, 'IWONA', 'MAJ', 1, 5, '2003-11-14', 3800, 140, '111222333'),
       (6, 'MONIKA', 'LIPA', 1, 5, '2000-09-02', 3400, 120, '444555666'),
       (7, 'ANNA', 'NOWAK', 3, 6, '1989-01-01', 2500, 0, '753951258'),
       (8, 'WANDA', 'DROGA', 3, 6, '2006-03-22', 2500, 50, '456258741');


-- TABELA SPECJALIZACJE

CREATE TABLE specjalizacje (
  id                INT         NOT NULL,
  specjalizacja     VARCHAR(25) NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO specjalizacje
VALUES (1, 'GINEKOLOG'),
       (2, 'CHIRURG'),
       (3, 'ANESTEZJOLOG'),
       (4, 'OKULISTA'),
       (5, 'LARYNGOLOG'),
       (6, 'KARDIOLOG'),
       (7, 'DERMATOLOG'),
       (8, 'GASTROLOG');


-- TABELA LEKARZE

CREATE TABLE lekarze (
  id                INT           NOT NULL,
  imie              VARCHAR(15)   NOT NULL,
  nazwisko          VARCHAR(20)   NOT NULL,
  id_specjalizacja  INT           NOT NULL,
  id_gabinet        INT           NOT NULL,
  data_zatr         DATE          NOT NULL,    
  pensja            DECIMAL(8,2),
  dodatek           DECIMAL(8,2),
  telefon           VARCHAR(16),
  PRIMARY KEY(id),
  FOREIGN KEY(id_specjalizacja) REFERENCES specjalizacje(id),
  FOREIGN KEY(id_gabinet) REFERENCES sale(id)
);

INSERT INTO lekarze
VALUES (1, 'MAGDALENA', 'GRZYB', 1, 2, '2010-03-11', 5500, 150, '457845222'),
       (2, 'RYSZARD', 'SKALPEL', 2, 4, '2008-04-01', 8000, 200, '456985365'),
       (3, 'ANNA', 'SEN', 3, 7, '2013-07-15', 10000, 120, '478598777'),
       (4, 'BARTLOMIEJ', 'OCZKO', 4, 8, '2007-09-01', 6700, 90, '445588996'),
       (5, 'MONIKA', 'USZATA', 5, 9, '2005-06-18', 7520, 220, '745896325'),
       (6, 'RYSZARD', 'SERCE', 6, 10, '2001-01-02', 7000, 200, '125698756'),
       (7, 'PIOTR', 'SKORA', 7, 11, '1989-03-01', 8100, 150, '145875632'),
       (8, 'KATARZYNA', 'ZOLADEK', 8, 12, '2016-12-01', 7650, 110, '654875654');


-- TABELA PACJENCI

CREATE TABLE pacjenci (
  id                INT         NOT NULL,
  imie              VARCHAR(15) NOT NULL,
  nazwisko          VARCHAR(20) NOT NULL,
  id_lekarz_prow    INT         NOT NULL,
  pesel             BIGINT      NOT NULL,
  data_rejestr      DATE        NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY(id_lekarz_prow) REFERENCES lekarze(id)
);

INSERT INTO pacjenci
VALUES (1, 'KATARZYNA', 'MUCHOMOR', 1, 82120544785, '2005-11-12'),
       (2, 'ADAM', 'NOGALSKI', 2, 80040111256, '2015-05-04'),
       (3, 'KRZYSZTOF', 'CHORY', 8, 89120511445, '2012-01-01'),
       (4, 'BOZYDAR', 'GIENKOWSKI', 5, 84111411456, '2009-07-23'),
       (5, 'ZYGFRYD', 'OLANY', 2, 92060415948, '2000-11-12');
       

-- TABELA OPERACJE

CREATE TABLE operacje (
  id               INT         NOT NULL,
  id_pacjent       INT         NOT NULL,
  id_lekarz        INT         NOT NULL,
  id_sala          INT         NOT NULL,
  data_op          TIMESTAMP   NOT NULL,
  opis             VARCHAR(50) NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY(id_pacjent) REFERENCES pacjenci(id),
  FOREIGN KEY(id_lekarz) REFERENCES lekarze(id),
  FOREIGN KEY(id_sala) REFERENCES sale(id)
);

INSERT INTO operacje
VALUES (1, 2, 2, 3, '2016-01-15 12:30', 'AMPUTACJA LEWEJ NOGI'),
       (2, 1, 1, 2, '2011-10-10 11:00', 'ZABIEG GINEKOLOGICZNY'),
       (3, 3, 8, 12, '2016-07-29 10:30', 'PLUKANIE ZOLADKA'),
       (4, 4, 5, 3, '2016-05-15 14:00', 'USUNIECIE PRZEGRODY NOSOWEJ'),
       (5, 5, 2, 3, '2016-07-14 13:30', 'PRZESZCZEP NERKI'),
       (6, 2, 2, 3, '2016-05-03 09:00', 'AMPUTACJA PRAWEJ NOGI');


-- TABELA LEKI

CREATE TABLE leki (
  id               INT         NOT NULL,
  nazwa            VARCHAR(25) NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO leki
VALUES (1, 'ASPIRYNA'),
       (2, 'APAP'),
       (3, 'WITAMINA C'),
       (4, 'STOPERAN');


-- TABELA RECEPTY

CREATE TABLE recepty (
  id               INT  NOT NULL,
  id_pacjent       INT  NOT NULL,
  id_lekarz        INT  NOT NULL,
  id_lek           INT  NOT NULL,
  data_przep       DATE NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY(id_pacjent) REFERENCES pacjenci(id),
  FOREIGN KEY(id_lekarz) REFERENCES lekarze(id),
  FOREIGN KEY(id_lek) REFERENCES leki(id)
);

INSERT INTO recepty
VALUES (1, 2, 2, 4, '2016-01-15'),
       (2, 1, 1, 4, '2011-10-10'),
       (3, 3, 8, 2, '2016-07-29'),
       (4, 4, 5, 1, '2016-05-15'),
       (5, 5, 2, 3, '2016-07-14'),
       (6, 2, 2, 4, '2016-05-03');
       

-- TABELA PRZYJECIA

CREATE TABLE przyjecia (
  id               INT       NOT NULL,
  id_pacjent       INT       NOT NULL,
  data_przyj       TIMESTAMP NOT NULL,
  data_wyp         TIMESTAMP NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY(id_pacjent) REFERENCES pacjenci(id)
);

INSERT INTO przyjecia
VALUES (1, 2, '2016-01-14 07:30', '2016-01-22 15:30'),
       (2, 1, '2011-10-10 07:00', '2011-10-11 14:00'),
       (3, 3, '2016-07-29 07:30', '2016-07-29 15:30'),
       (4, 4, '2016-05-15 07:00', '2016-05-16 11:00'),
       (5, 5, '2016-07-14 07:30', '2016-07-30 14:30'),
       (6, 2, '2016-05-03 07:00', '2016-05-10 15:00');
