var myCenter = new google.maps.LatLng(54.381278, 18.5882991);

function initialize() {
	var mapProp = {
		center : myCenter,
		zoom : 14,
		scrollwheel : false,
		draggable : false,
		mapTypeId:google.maps.MapTypeId.ROADMAP   //lub .HYBRID  lub .SATELLITE
	};

	var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

	var marker = new google.maps.Marker({
		position : myCenter,
	});

	marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);