var bd = [
	{'brand' : 'Ford', 'type': 'Focus Station', 'photo' : 'ford_focus_station.png', 'pricePerDay': 120, 'doors' : 5, 'people' : 5, 'AC' : false, 'gearbox' : 'ręczna', 'specialOffer' : false},
	{'brand' : 'Ford', 'type': 'Focus Station', 'photo' : 'ford_focus_station.png', 'pricePerDay': 135, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'auto', 'specialOffer' : true},
	{'brand' : 'Ford', 'type': 'Galaxy TDCI', 'photo' : 'ford_galaxy_TDCI.png', 'pricePerDay': 130, 'doors' : 5, 'people' : 7, 'AC' : true, 'gearbox' : 'auto', 'specialOffer' : true},
	{'brand' : 'Ford', 'type': 'Tourneo Connect', 'photo' : 'ford_tourneo_connect.png', 'pricePerDay': 125, 'doors' : 5, 'people' : 7, 'AC' : false, 'gearbox' : 'ręczna', 'specialOffer' : false},
	{'brand' : 'Ford', 'type': 'Tourneo Connect', 'photo' : 'ford_tourneo_connect.png', 'pricePerDay': 145, 'doors' : 5, 'people' : 7, 'AC' : true, 'gearbox' : 'auto', 'specialOffer' : false},

	{'brand' : 'Hyundai', 'type': 'i10', 'photo' : 'hyundai_i10.png', 'pricePerDay': 130, 'doors' : 5, 'people' : 4, 'AC' : false, 'gearbox' : 'ręczna', 'specialOffer' : true},
	{'brand' : 'Hyundai', 'type': 'i10', 'photo' : 'hyundai_i10.png', 'pricePerDay': 150, 'doors' : 5, 'people' : 4, 'AC' : true, 'gearbox' : 'auto', 'specialOffer' : false},

	{'brand' : 'Mercedes', 'type': 'C-Class', 'photo' : 'mercedes_C-Class.png', 'pricePerDay': 160, 'doors' : 4, 'people' : 5, 'AC' : true, 'gearbox' : 'ręczna', 'specialOffer' : false},
	{'brand' : 'Mercedes', 'type': 'C-Class', 'photo' : 'mercedes_C-Class.png', 'pricePerDay': 180, 'doors' : 4, 'people' : 5, 'AC' : true, 'gearbox' : 'auto', 'specialOffer' : true},

	{'brand' : 'Nissan', 'type': 'Qashqai', 'photo' : 'nissan_qashqai.png', 'pricePerDay': 100, 'doors' : 5, 'people' : 5, 'AC' : false, 'gearbox' : 'ręczna', 'specialOffer' : false},
	{'brand' : 'Nissan', 'type': 'Qashqai', 'photo' : 'nissan_qashqai.png', 'pricePerDay': 125, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'auto', 'specialOffer' : true},

	{'brand' : 'Opel', 'type': 'Astra', 'photo' : 'opel_astra.png', 'pricePerDay': 105, 'doors' : 4, 'people' : 5, 'AC' : true, 'gearbox' : 'ręczna', 'specialOffer' : true},
	{'brand' : 'Opel', 'type': 'Astra', 'photo' : 'opel_astra.png', 'pricePerDay': 115, 'doors' : 4, 'people' : 5, 'AC' : true, 'gearbox' : 'auto', 'specialOffer' : false},

	{'brand' : 'Renault', 'type': 'Traffic', 'photo' : 'ranault_trafic.png', 'pricePerDay': 120, 'doors' : 4, 'people' : 9, 'AC' : true, 'gearbox' : 'ręczna', 'specialOffer' : false},
	{'brand' : 'Renault', 'type': 'Clio', 'photo' : 'renault_clio.png', 'pricePerDay': 90, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'ręczna', 'specialOffer' : true},
	{'brand' : 'Renault', 'type': 'Clio', 'photo' : 'renault_clio.png', 'pricePerDay': 85, 'doors' : 5, 'people' : 5, 'AC' : false, 'gearbox' : 'ręczna', 'specialOffer' : true},
	{'brand' : 'Renault', 'type': 'Megane', 'photo' : 'renault_megane.png', 'pricePerDay': 130, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'ręczna', 'specialOffer' : false},

	{'brand' : 'Skoda', 'type': 'Octavia', 'photo' : 'skoda_octavia.png', 'pricePerDay': 130, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'auto', 'specialOffer' : false},
	{'brand' : 'Skoda', 'type': 'Octavia', 'photo' : 'skoda_octavia_stat.png', 'pricePerDay': 110, 'doors' : 5, 'people' : 5, 'AC' : false, 'gearbox' : 'ręczna', 'specialOffer' : true},

	{'brand' : 'Volvo', 'type': 'XC60', 'photo' : 'volvo_XC60.png', 'pricePerDay': 135, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'ręczna', 'specialOffer' : true},
	{'brand' : 'Volvo', 'type': 'XC60', 'photo' : 'volvo_XC60.png', 'pricePerDay': 155, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'auto', 'specialOffer' : false},

	{'brand' : 'VW', 'type': 'Passat', 'photo' : 'vw_passat.png', 'pricePerDay': 130, 'doors' : 4, 'people' : 5, 'AC' : true, 'gearbox' : 'auto', 'specialOffer' : false},
	{'brand' : 'VW', 'type': 'Passat', 'photo' : 'vw_passat.png', 'pricePerDay': 120, 'doors' : 4, 'people' : 5, 'AC' : false, 'gearbox' : 'auto', 'specialOffer' : true},
	{'brand' : 'VW', 'type': 'Passat', 'photo' : 'vw_passat2.png', 'pricePerDay': 110, 'doors' : 5, 'people' : 5, 'AC' : false, 'gearbox' : 'ręczna', 'specialOffer' : true},
	{'brand' : 'VW', 'type': 'Passat', 'photo' : 'vw_passat2.png', 'pricePerDay': 130, 'doors' : 5, 'people' : 5, 'AC' : true, 'gearbox' : 'auto', 'specialOffer' : false}
];


function fillOfferList(arr){
	var carList = $('#oferta .cars');
	carList.empty();

	arr.forEach(function(car){	
		var singleCar; 
		if (car["specialOffer"]) {
			singleCar = $('<div class="car" data-special="true"></div>');
		} else {
			singleCar = $('<div class="car"></div>');
		}

		var carName = $('<h4>' + car["brand"] + ' ' + car["type"] + '</h4>');
		var deal = $('<p><span class="deal">Super oferta!</span></p>');
		var carPhoto = $('<img src="img/cars/' + car["photo"] + '"/>');
		var pricePerDay = $('<p>Cena za dobę: <span class="dayPrice">' + car["pricePerDay"] + '</span>zł</p>');
		var numbOfDays = $('<p><label for="days">Liczba dni:&nbsp;&nbsp;</label><input type="number" class="days" id="days" value="1" min="0" max="365"></p>');
		var totalPrice = $('<p>Całkowita kwota: <span class="total">' + car["pricePerDay"] + '</span> zł</p>');
		var btnInfo = $('<button class="btnInfo"><span class="glyphicon glyphicon-info-sign"></span><br />Szczegóły</button>');

		var divTag = $('<div class="moreInfo"></div>');
		var ac = car["AC"] ? 'tak' : 'nie';
		var features = $('<table><tr><td>drzwi</td><td>' + car["doors"] + '</td></tr><tr><td>pasażerowie</td><td>' + car["people"] + '</td></tr><tr><td>klimatyzacja</td><td>' + ac + '</td></tr><tr><td>s. biegów</td><td>' + car["gearbox"] + '</td></tr></table> ');

		$(divTag).append(features);

		$(singleCar).append(carName).append(deal).append(carPhoto).append(pricePerDay).append(numbOfDays).append(totalPrice).append(btnInfo).append(divTag);
		
		var carInCol = $('<div class="col-sm-6 col-md-4 col-lg-4 carCol"></div>');

		$(carInCol).append(singleCar);

		$(carList).append(carInCol);
	});
}


$(document).ready(function() {

	fillOfferList(bd);

	$('#oferta').on('click', '#filter', function() {
		var filteredBD = bd;
		//filter by AC
		if ($('#oferta input[name="AC"]').is(':checked')) {
			filteredBD = filteredBD.filter(function(car) {
				return car["AC"];
			});
		}
		//filter by gearbox
		if ($('#oferta input[value="manual"]').is(':checked')) {
			filteredBD = filteredBD.filter(function(car) {
				return car["gearbox"] == 'ręczna';
			});
		}
		else if ($('#oferta input[value="automatic"]').is(':checked')) {
			filteredBD = filteredBD.filter(function(car) {
				return car["gearbox"] == 'auto';
			});
		}
		//filter by text input
		var searchText = $('#searchCar');
		if (searchText.val() != '') {
			filteredBD = filteredBD.filter(function(car) {
				var matcher = new RegExp(searchText.val(), "gi");
				// console.log("***");
				// console.log(car["brand"]+" "+car["type"]+" wyszukiwane: "+searchText.val());
				return (matcher.test(car["brand"]) || matcher.test(car["type"]));
			});
		}

		fillOfferList(filteredBD);
	});
	



	$('#oferta .cars').on('keyup mouseup', '.days', function() {
		var dayPrice = +$(this).closest('.car').find('.dayPrice').html();
		var dayNumb = $(this).val();
		$(this).closest('.car').find('.total').html(dayNumb * dayPrice);
	});

	$('#oferta .cars').on('click', '.btnInfo', function() {
		$(this).css('display', 'none');
		$(this).closest('.car').find('.moreInfo').fadeIn();
	});

	$('#oferta .cars').on('mouseenter', '.car', function() {
		if($(this).data('special')) {
			$(this).find('.deal').animate({'top' : '-10px', 'opacity': '1',}, 'fast');		
		}
	});

	$('#oferta .cars').on('mouseleave', '.car', function() {
		$(this).find('.deal').animate({'top' : '0px', 'opacity': '0',}, 'fast');		
	});

	$(document).keypress(function(e) {
	    if(e.which == 13) {
	       $('#oferta #filter').click();
	    }
	});


});