$(document).ready(function() {
   
    console.log("--APP START--");
    
    $('#startGame').click(function() {
        var rangeFrom = parseInt($('#rangeFrom').val());
        var rangeTo = parseInt($('#rangeTo').val());
        var lives = parseInt($('#lives').val());
        var randomizedNumber;
        var guessTries = 1;
        var $heading = $('h1');     // gdy caly element pobrany selektorem, to w jego nazwie przypisac $ - dobra praktyka!
        var $subHeading = $('h4');
        
        if (rangeFrom>0 && rangeTo>rangeFrom && lives>0) {
 
            $('.settingsBox').css('display', 'none');
            $('.playBox').css('display', 'block');
            
            randomizedNumber = (Math.random() * (rangeTo - rangeFrom) + rangeFrom + 1) | 0;
            console.log('Wylosowany numer to: ' + randomizedNumber);
            
            $('#guessButton').click(function() {
                          
                var $guessNumber = $('#guessNumber');   
                
                if (guessTries < lives) {
                    if (randomizedNumber == parseInt($guessNumber.val())) {
                        $heading.html('BRAWO!!!<br />Wygrałeś! :)').css('color', 'green');
                        $('.playBox').css('display', 'none');
                        $subHeading.css('display', 'none');
//                        $subHeading.html('Zgadłeś w ' + (guessTries - 1)+ ' próbach.');
                    } 
                    else {
                        
                        if (parseInt($guessNumber.val()) > randomizedNumber) {
                            $heading.html('Podana liczba jest większa od wylosowanej!').css({"color": "green", "font-size": "15pt"});
                        }
                        else {
                            $heading.html('Podana liczba jest mniejsza od wylosowanej!').css({"color": "green", "font-size": "15pt"});
                        }
                    }  
                    guessTries ++;
                }
                else {
                    $heading.html('Przegrana! <br/>Koniec gry.').css('color', 'red');
                    $subHeading.html('');
                }
                $subHeading.html('Próba ' + guessTries + " / " + lives);
            });   
            $subHeading.html('Próba ' + guessTries + " / " + lives);
        }
        else {
            console.log("Podano złe ustawienia!");
        }        
    });    
});